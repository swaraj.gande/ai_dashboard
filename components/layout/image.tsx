import styles from "../../styles/Components.module.scss";
import Pwc from '../../public/PwC.jpg'
export function ImageForLogo(props: { icon: any }) {
  return (
    <div className={styles.icon}>
      <img style={{
        width:'50px',
        marginRight:'30px'
      }} src={props.icon.src} />
    </div>
  );
}